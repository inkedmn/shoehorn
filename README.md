This is a little bullshit thing I'm writing to take Scrivener's HTML export output and get rid of all of the styles and other cruft so it can easily be added to Wordpress.

Bonus points if I can automate the following things (this will all be done post-launch because duh):

1. Inventory images and other assets and automatically upload them to WP, get the URL where they live, and insert that into the text.
2. Create the pages in WP (probably impossible to set the Ontraport permissions this way, so this is likely not going to happen).