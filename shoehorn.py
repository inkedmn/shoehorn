#!/usr/bin/env python

# You won't believe how much HTML parsing I do with 
# regular expressions here. Enough to really annoy any
# real programmer, that's for sure.


##
# TODO:
# 1. Have it pull everything between <body> instead of this
# chopping it one end at a time like some kind of dumbass.
# 2. Replace the metacharacters before dumping the output
# 3. See if you can't reasonably name the output files instead of
# using numbers like an idiot.
# 4. Actually, just use BeautifulSoup for all of this garbage and
# stop embarrassing yourself in front of the Internet.
# 5. Upload images, etc. to CDN as part of output process (and track upload
# status by recording checksums of uploaded content)
# 6. 
##

from time import strftime
import hashlib
import json
import md5
import os.path
import pyrax
import re
import sys
import urllib
import urlparse

APIKEY = file("~/.rackspace_key").read().strip()

###
# Text handling
###

# Look, we play the hand we're dealt and I had to
# sort out a non-stupid way to indiciate chapter breaks
# within Scrivener and this does the job so shut up
SPLIT_AT = '<p class="p3">PAGEBREAK</p>'
EMPTIES = [
        '<p class="p\d"><br></p>',
        '<span class="Apple-converted-space">\s+</span>'
        ]

## Replace these with their HTML entity counterparts
## so we don't look like a bunch of idiots.
CHAR_MAP = {
        'Cmd'   : '&#x2318;',
        'Opt'   : '&#x2325;',
        'Shift' : '&#x21E7;',
        'Ctrl'  : '&#x2303;',
        '...'   : '&hellip;',
        '--'    : '&mdash;',
}


class AssetUploadFailureException(Exception): pass

class Book(object):

    def __init__(self, html)
        self.html = html
        self.chapters = []

    def doSourceClean(self):
        self._trimHead()
        self._removeEmptyParagraphs()
        self._unsetUnwantedAttributes()

    def _trimHead(self):
        self.html = HtmlHelper.removeElements(self.html, ['head']) 

    def _removeEmptyParagraphs(self):
        """Remove the empty paragraphs that Scrivener craps out"""
        for e in EMPTIES:
            reg = re.compile(e, re.MULTILINE)
            self.html = reg.sub('', html)

    def _unsetUnwantedAttributes(self):
        elements = ['p','img','ul','ol','a']
        attrs = ['id','class','width','height']
        self.html = HtmlHelper.unsetElementAttributes(html, elements, attrs)

    def separateIntoChapters(self):
        pages = self.html.split(SPLIT_AT)
        for page in pages:
            c = Chapter(page)
            self.chapters.append[c]


class Chapter(object):

    def __init__(self, html):
        self.html = html
    
    def 
    

class ImageSwapper(object):
    """Utility class for dealing with embedded images"""

    def __init__(self, apikey, assetManager):
        self.apikey = apikey
        self.assetManager = assetManager
        self.assetUploader = AssetUploader(self.apikey, self.assetManager)

    def replaceOrUploadImage(self, path):
        a = Asset(path)
        if self.assetUploader.imageNeedsUpdate(a.fpath)):
            a = self.assetUploader.uploadAsset(a)
        if a.uploaded:
            return a.publicUrl 
        raise AssetUploadFailureException("Asset failed to upload: %s" % \
                a.fname)
        # this will be a path relative to the document
        # i.e., "Images/PoopFart.png"



class HtmlHelper(object):

    @staticmethod
    def removeElements(html, elements):
        if not isinstance(elements, list):
            elements = [elements]
        soup = BeautifulSoup(html)
        for element in elements:
            for matches in soup.find_all('^%s$' % element)):
                # peace out, mf
                del[element]
        return str(soup) 

    @staticmethod
    def unsetElementAttributes(html, elements, attributes):
        """Remove inline attributes from supplied element types"""
        soup = BeautifulSoup(html)
        for e in elements:
            els = soup.find_all(re.compile('^%s$' % e))
            for el in els:
                for attr in attributes:
                    try:
                        del el[attr]
                    except KeyError:
                        continue
                    except Exception, e:
                        print "Error removing tag"
                        print e
        return str(soup)


###
# Asset management and uploading
###

DEFAULT_CONTAINER = "eeassets"
MAP_FILE = os.path.expanduser("~/.eeassetmap")

class Asset(object):
    """Represents an individual asset"""

    def __init__(self, fpath):
        if not os.path.exists(fpath):
            raise IOError("File not found: %s" % fpath)
        self.fpath = fpath
        self.fname = os.path.basename(fpath)
        self.uploaded = False
        self.publicUrl = ''
    
    @property
    def name(self):
        return self.fname

    @property
    def md5(self):
        """Returns the MD5 checksum of the file"""
        # totally jacked this one-liner from SO, fyi. 
        # Creative Commons WHUT
        # http://stackoverflow.com/a/3431835/60664
        return hashlib.md5(open(self.fpath, "rb").read()).hexdigest()

    @property
    def data(self):
        """Returns byte array of file contents"""
        return file(self.fpath, 'rb').read() 

    @property
    def asHtml(self):
        s = "<img src=\"%s\" alt=\"%s\" />" % (self.publicUrl, \
                self.fname[:3])
        return s


class AssetMap(object):
    """Creates a map of assets to md5 checksums"""
    def __init__(self, fpath=MAP_FILE):
        self.fpath = fpath
        self.map = {}
        if(os.path.exists(fpath)):
            self._load()

    def save(self):
        with file(self.fpath, 'w') as fd:
            fd.write(json.dumps(self.map))
    
    def _load(self):
        raw = file(self.fpath).read().strip()
        if raw: 
            self.map = json.loads(raw)

    def addEntry(self, fname, checksum):
        self.map[fname] = checksum

    def removeEntry(self, fname):
        del(self.map[fname])

    def getEntry(self, fname):
        try:
            return self.map[fname]
        except KeyError:
            return False

    def __repr__(self):
        out = ''
        for k in self.map:
            out += "%s : %s" % (k, self.map[k])
            out += '\n'
        return out


class AssetUploader(object):
    """Client class for Rackspace CDN"""

    def __init__(self, apikey, assetMap, container=DEFAULT_CONTAINER):
        """set up the rackspace client instance, etc."""
        self.assetMap = assetMap
        pyrax.set_setting("identity_type", "rackspace")
        pyrax.set_default_region("DFW")
        pyrax.set_credentials("mrbrettkelly", apikey)
        self.container = pyrax.cloudfiles.get_container(DEFAULT_CONTAINER)

    def imageNeedsUpdate(self, fpath):
        """Checks if the supplied file is different from what's on the CDN"""
        # make a new Asset instance so it's easy to do all this crap
        a = Asset(fpath)
        # do we already have this file?
        cksum = self.assetManager.getEntry(a.fname)
        if cksum && a.md5 == cksum:
            # Nope, CDN has this image
            return False
        # Image is either different or missing altogether
        return True


    def uploadAsset(self, asset):
        """add asset to rackspace container"""
        obj = self.container.store_object(asset.name, asset.data)
        encName = urllib.quote(obj.name)
        asset.publicUrl = urlparse.urljoin(self.container.cdn_uri, encName)
        asset.uploaded = True
        # add it to the asset map once it's up
        self.assetMap.addEntry(asset.name, asset.md5)
        self.assetMap.save()
        return asset


##########




OUT_DIR = 'output'

def trimHeadCruft(html):
    """Remove unwanted elements"""
    # I know, I know. Shut up.
    reg = re.compile("^.*<body>",re.DOTALL|re.MULTILINE)
    out = reg.sub('', html)
    return out

def splitDoc(html):
    """Separate a single HTML document into individual documents"""
    if SPLIT_AT in html: 
       return html.split(SPLIT_AT)
    return html
    
def unsetElementAttributes(html, elements, attributes):
    """Remove inline attributes from supplied element types"""
    soup = BeautifulSoup(html)
    for e in elements:
        els = soup.find_all(re.compile('^%s$' % e))
        for el in els:
            for attr in attributes:
                try:
                    del el[attr]
                except KeyError:
                    continue
                except Exception, e:
                    print "Error removing tag"
                    print e
    return str(soup)

def removeEmptyParagraphs(html):
    """Remove the empty paragraphs that Scrivener craps out"""
    for e in EMPTIES:
        reg = re.compile(e, re.MULTILINE)
        html = reg.sub('', html)
    return html

def appendGenerationDate(html):
    cur = strftime("%Y-%m-%d %H:%M:%S")
    mark = "<!-- Generated: %s -->" % cur
    return html+mark

### Amateurish code by me

f = sys.argv[1]
if not os.path.exists(f):
    print "No such file"
    raise SystemExit
h = open(f).read()

h = trimHeadCruft(h)
pages = splitDoc(h)

pindex = 1

for page in pages:
    page = removeEmptyParagraphs(page)
    page = unsetElementAttributes(
            page, 
            ['p','img','ul','ol','a'], 
            ['id','class','width','height']) 
    page = appendGenerationDate(page)
    with open(os.path.join(OUT_DIR, ('%d.html' % pindex)), 'w') as fd:
        fd.write(page)
    pindex += 1

print "Wrote %d pages unless something shit the bed" % (pindex - 1)
